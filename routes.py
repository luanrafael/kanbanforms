#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'

from flask import Flask
from flask import Flask, request
import manager

import json
import sys
import officer
import threading
import logging
from models import helper, service, agenda

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

logger = logging.getLogger(__name__)

ROUTE_API_V1 = "/api/v1/"


@app.route(ROUTE_API_V1 + "service/<id>")
def show_service(id):
    s = service.get(id)

    if s is not None:
        return s.data

    return "nok"


@app.route(ROUTE_API_V1 + "run/<id>/<clinica>/<instance>/<task>")
def run_service(id, clinica, instance, task):

    man = manager.Manager()
    ret = man.proc(id, clinica, instance, task)

    log_filename = 'log_services/' + str(id) + '_' + str(instance) + '_' + task + '.log'
    f = open(log_filename, 'w', 99999, 'utf-8')
    f.write(ret)
    f.close()

    return "ok"


@app.route(ROUTE_API_V1 + "log/<id>/<instance>/<task>")
def log_service(id, instance, task):
    log_filename = 'log_services/' + str(id) + '_' + str(instance) + '_' + task + '.log'

    with open(log_filename, 'rU', 99999, 'utf-8') as data_file:
        data = data_file.read()

    return data.replace('\n', '<br/>')


@app.route(ROUTE_API_V1 + "config/<id>", methods=['POST', 'GET'])
def upd_json_config(id):

    form_template = '''
            <form method='POST'>
                <textarea name='jsonconfig' style="width: 90%;margin:0 auto" rows=10></textarea>
                <br>
                <button type='submit' style="background: #4caf50;width: 90%;height: 50px;margin: 0 auto;color: #fff;font-size: 20px;">Salvar</button>
            </form>
            <br>
            <h2 style="color:red"> {{ error_message }} </h2>
        '''

    if request.method == 'GET':
        return form_template.replace('{{ error_message }}', '')

    if request.method == 'POST':

        try:
            json.loads(request.form['jsonconfig'])
        except ValueError:
            return form_template.replace('{{ error_message }}', request.form['jsonconfig'] + '<br>Json Inválido')

        dbsession = helper.DBSession()

        s = service.Service(id,request.form['jsonconfig'])

        dbsession.save(s)

        output = dbsession.get(service.Service,id)

        if output is not None:
            return output.data

        dbsession.dispose()

        return output


@app.route(ROUTE_API_V1 + "agenda/<clinica>/<medico>", methods=['POST'])
def sinc_agenda(clinica, medico):
    logger.info("sinc_agenda %s - %s", clinica, medico)

    ultima_agenda = agenda.get_last(clinica, medico)

    agenda_atual = agenda.Agenda(medico, request.json, clinica)



    try:
        dbsession = helper.DBSession()
        dbsession.save(agenda_atual)
        output = {"error": False, "message": agenda_atual.id}
    except:
        output = {"error": True, "message": str(sys.exc_info())}

    if ultima_agenda is None:
        officer_thread = threading.Thread(target=officer.sincronizar_agenda,
                                          args=(clinica, medico, agenda_atual.consultas, "[]"))
    else:
        officer_thread = threading.Thread(target=officer.sincronizar_agenda,
                                          args=(clinica, medico, agenda_atual.consultas, ultima_agenda.consultas))

    officer_thread.daemon = True
    officer_thread.start()

    return json.dumps(output)


@app.route("/")
def home():
    return "ok"