__author__ = 'epsoft'
from sqlalchemy import *
import configparser

engines = {}

def get_engine(database='Database'):
    global engines

    if database not in engines:
        engine = create_engine(build_connection(database), echo=True)
        engines[database] = engine

    return engines[database]


def build_connection(database):
    config = configparser.ConfigParser()
    config.read('config.txt')
    con = config.get(database,'connection') # ['connection']
    user = config.get(database,'user') # ['user']
    password = config.get(database,'pass') #['pass']
    host = config.get(database,'host') #['host']
    port = config.get(database,'port') #['port']
    schemma = config.get(database,'schemma') #['schemma']
    url = con + user + ':' + password + '@' + host + ':' + port + '/' + schemma
    return url.replace(' ', "")
